import bpy
import time     # For adding the time and date to the filenames.
from os.path import join

def renderSingleImage(scene, resolutionX, resolutionY, foldername, filename):
#def renderSingleImage(scene, resolutionX, resolutionY, filename):
    """
    Also number of samples in Cycles maybe?
    And denoising option?
    Also filmic space and stuff?
    """

    scene.render.image_settings.color_mode = 'RGB'
    scene.render.engine = 'CYCLES'

    # Double slash == Parent directory
    scene.render.filepath = join(foldername, filename)
    #scene.render.filepath = join("//Renders/", filename)

    scene.render.image_settings.file_format = 'PNG'
    scene.render.resolution_x = resolutionX
    scene.render.resolution_y = resolutionY

    bpy.ops.render.render(write_still=True)

def renderAnimation(scene, resolutionX, resolutionY, foldername, filename):
    """
    Renders the animation into a folder named after the target name, time and date, 
    every frame a picture

    New strategy: this function calls the function above (renderSingleImage()) in a loop.
    """

    scene.render.image_settings.color_mode = 'RGB'
    scene.render.engine = 'CYCLES'

    scene.render.image_settings.file_format = 'PNG'
    scene.render.resolution_x = resolutionX
    scene.render.resolution_y = resolutionY
    
    # Old method: simply rendering the animation. Blender added numbers after the
    # filenames, and there was no way around that.

    #scene.render.filepath = join("//Renders/", foldername, filename) 
    #bpy.ops.render.render(animation = True)

    # New method: calling renderSingleImage() on a loop.
    # Now I have total control over the filenames.

    for frame in range(scene.frame_start, scene.frame_end + 1):
        scene.frame_set(frame)
        #renderSingleImage(scene, resolutionX, resolutionY, foldername, filename)
        renderSingleImage(scene, resolutionX, resolutionY, foldername, filename + "-" + str(frame))

def renderMultispectralAnimation(scene, resolutionX, resolutionY, targetName):
    """
    Renders each frame of the animation with a different albedo.
    Wait a minute, it may be easier to call renderAnimation() multiple times.
    """
    pass
