Usage: run main.py in Python 3, and if you agree with the settings, accept and run. The results will be saved in /Renders. In order to change the settings, head to main.py and edit them manually at the beginning of the file.

Check the [project wiki](https://bitbucket.org/mariofpalos/asteroid-image-generator/wiki/Home) for more detailed information.

If you wish to skip the confirmation step, add **-s** or **--skipConfirmation** to the command. If you want the script to print all the settings but not render, use **-p** or **--justPrint**

You will need to write the full path to your Blender executable inside main.py the first time you use it.

Copyright 2021 Mario F. Palos, Antti Penttil�, Tomas Kohout (Institute of Geology of the Czech Academy of Sciences and University of Helsinki)
Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated documentation files (the "Software"), to deal in the Software without restriction, including without limitation the rights to use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of the Software, and to permit persons to whom the Software is furnished to do so, subject to the following conditions:
The above copyright notice and this permission notice shall be included in all copies or substantial portions of the Software.
THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.