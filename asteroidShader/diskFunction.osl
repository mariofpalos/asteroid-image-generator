//Remember to update the Script node in Blender after every change in the code.
//All angles in radians.

///// Definition of disk functions /////

float LommelSeeliger (float i, float e) {

	return cos(i) / ( cos(i) + cos(e) );

}

float ROLO (float i, float e) {
	//Same as Lommel-Seeliger

	return LommelSeeliger(i, e);

}

float Akimov (float i, float e) {

	float alpha = i + e;

	float l = atan( ( ( cos(i) / cos(e) ) - cos(alpha) ) / sin(alpha) );

	//No need to calculate b explicitely because only its cosine is used.
	float cosb = cos(e) / cos(l);

	float chunk1 = cos( alpha/2 );
	
	float chunk2 = cos( ( M_PI / (M_PI - alpha) ) * ( l - ( alpha / 2 ) ) );

	float chunk3 = pow( cosb , ( alpha / ( M_PI - alpha ) ) ) / cos(l);

	return chunk1 * chunk2 * chunk3;

}

float LinearAkimov (float i, float e) {
	//Linear-Akimov's disk function is the same as Akimov.

	return Akimov(i, e);

}

float LunarLambert (float i, float e, float epsilon, float zeta, float eta) {

	float alpha = i + e;

	float exponent = (epsilon * alpha) + (zeta * pow(alpha, 2)) + (eta * pow(alpha, 3));	

	float L = exp(exponent);
	
	return 2 * L * ( cos(i) / (cos(i) + cos(e)) ) + (1 - L) * cos(i);

}

//float Minnaert (float i, float e) {
float Minnaert (float i, float e, float k_0, float b) {

	float alpha = i + e;
	
	float k = k_0 + b * alpha;

	return pow(cos(i), k) * pow( cos(e), (k - 1) );

}

///// Shader /////

shader diskFunction (

	//Use semicolons in the body and commas here.

	//All variables need a default value!

	//Incidence angle (i).
	float i = 0.0,	

	//Emission angle (e).
	float e = 0.0,

	//Photometric function
	int function = 1,

	//Filter
	//Only used for Lunar-Lambert and Minnaert
	int filter = 1,

	//Output shader
	//output closure color BSDF = diffuse(N)

	//Output value of the disk function
	output float Output = 0.0,
	
	//And the alpha angle to be used in the phase function
	output float alpha = 0.0

)

{

	alpha = i + e;

	if (function == 1){
		//Lommel-Seeliger

		Output = LommelSeeliger(i, e);

	}

	else if (function == 2) {
		//ROLO, its disk function is the same as Lommel-Seeliger.

		Output = ROLO(i, e);

	}

	else if (function == 3) {
		//Akimov

		Output = Akimov(i, e);

	}

	else if (function == 4) {
		//Linear Akimov. Its disk function is the same as Akimov.
		
		Output = LinearAkimov(i, e);

	}
	
	else if (function == 5) {
		//Lunar-Lambert
		//This phase function depends in principle on the filter, but in the paper
		//all the values for epsilon, zeta, and eta are the same.

		Output = LunarLambert(i, e, -0.009, 0, 0);

	}
	
	else if (function == 6) {
		//Minnaert
		//Like Lunar-Lambert, this function has two parameters (k_0 and b), but
		//they have the same value for every filter in the paper.

		Output = Minnaert(i, e, 5.3e-1, 2.1e-3);

	}

	else {
	
		//Maybe output something super silly to represent an error.
		//I wanted to output a color, but it has to be grayscale.
		//Maybe a weird pattern?

		Output = 0.0;

	}
		
}
