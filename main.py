import sys
import os
import subprocess
import argparse

# Useful: https://docs.blender.org/api/blender_python_api_current/info_tips_and_tricks.html

#### modifier = bpy.data.objects['ryugu_800k'].modifiers["GeometryNodes"]
#### modifier.show_render = True / False

#----------------------------------------------------------------------------------#

"""
Copyright 2021 Mario Palos, Antti Penttilä, Tomas Kohout (Institute of Geology of the Czech Academy of Sciences and University of Helsinki)
Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated documentation files (the "Software"), to deal in the Software without restriction, including without limitation the rights to use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of the Software, and to permit persons to whom the Software is furnished to do so, subject to the following conditions:
The above copyright notice and this permission notice shall be included in all copies or substantial portions of the Software.
THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
"""

#----------------------------------------------------------------------------------#

# USER OPTIONS

##### Paths #####

# Blender 2.93LTS
blenderPath         =   "/home/mario/Programs/blender-2.93/./blender"


positionalDataPath  =   "data/positionalData.txt"
#positionalDataPath  =   "data/fullRotation.txt"


##### Coordinates #####

readFromFile        =   True 

# If readFromFile == False, the following values will be used:
sunCoordinates      =   [3, -7, 6]    # km 
cameraCoordinates   =   [4, 4, 3]   # km


##### Photometric function #####

function            =   6
# Options: 1: Lommel-Seeliger, 2: ROLO, 3: Akimov, 4: Linear-Akimov, 5: Lunar-Lambert, 6: Minnaert


##### Target selection #####

target              =   3
# Options:  1: DRAv2.22 (Didymos & Dimorphos)       2: BISv1.0 (Bennu & Itokawa, scaled)
#           3: RISv1.0  (Bennu & Itokawa, scaled)   4: Plain sphere


##### Procedural detail #####

proceduralDetail    =   True

##### System rotation #####

systemRotation      =   90  # degrees   # Only used if not readFromFile

##### Calibration #####

calibration         =   False            # Creates a calibration image for every render


##### Camera #####

camera              =   "ASPECT-VIS"
# Options: "ASPECT-VIS", "ASPECT-NIR", "ASPECT-VIS-GREEN", or "False".
# Need a better solution than "False" for this.

# If camera == False, the following settings will be used:
resolutionX         =   512     # pixels
resolutionY         =   512     # pixels
fovX                =   .15     # radians
fovY                =   .15     # radians


##### Multispectral rendering #####

multispectral       =   False            # Render every image or sequence in multiple wavelengths.

# If multispectral == True and no real camera is selected:
wavelengthRange     =   [500, 1000, 50] # nm

# If multispectral == False, the following wavelength will be used:
defaultWavelength   =   1000 # nm
#defaultWavelength   =   547 # nm


##### Sun irradiance #####

sunIrradiance       =   250 # W·m^-2


#----------------------------------------------------------------------------------#

# ARGUMENT PARSING
# Processes the options (only --skipConfirmation for now)

argv = sys.argv

parser = argparse.ArgumentParser()

parser.add_argument("-s", "--skipConfirmation", dest = "skipConfirmation", action = "store_true",
        required = False, help = "Skips the user confirmation step.")

parser.add_argument("-p", "--justPrint", dest = "justPrint", action = "store_true",
        required = False, help = "Do not run Blender, just print what would be the command.")

args = parser.parse_args()

#----------------------------------------------------------------------------------#

# Parametric functions.

functionList = [
        "Lommel-Seeliger",
        "ROLO",
        "Akimov",
        "Linear-Akimov",
        "Lunar-Lambert",
        "Minnaert"]

# Targets

targetList = [
        "DRAv2.22",
        "BISv1.0",
        "RISv1.0",
        "Sphere",
        ]

targetName = targetList[target - 1]

#----------------------------------------------------------------------------------#

# Confirmation menu

if not args.justPrint:

    print("\nCurrent settings (modify them in main.py if you don't agree):\n")

    ## Paths.

    print("* Blender path: " + blenderPath)
    print("* Positional data path: " + positionalDataPath)

    print("")

    ## File / User input.

    if readFromFile:
        print("* Reading positional data from file " + positionalDataPath)
    else:
        print("* Using user-defined coordinates.")
        print("** Setting the sun at " + str(sunCoordinates) +
                " and the camera at " + str(cameraCoordinates) + ".")


    print("* Photometric function selected: " + str(function) + 
            " (" + functionList[function - 1] + ").")

    ## Multispectral render option.

    if multispectral: #and not albedoFile:
        print("* Rendering wavelengths in the range (" + str(wavelengthRange[0]) + ", " + 
                str(wavelengthRange[1]) + ") nm in steps of " + str(wavelengthRange[2]) + " nm.")

#    else:
    if not multispectral and camera == False:
        print("* Using default albedo value.")

    ## Target name

    print("* Selected target: " + targetName + ".")

    ## Procedural detail

    print("* Procedural detail state: " + str(proceduralDetail) + ".")

    ## System rotation

    print("* System rotation: " + str(systemRotation) + " degrees.")

    ## Calibration option

    if calibration:
        print("* Rendering calibration images")
    else:
        print("* No calibration disk")

    ## Camera parameters

    print("")
    print("Camera settings:")
    print("")

    if camera:
        print("* Camera in use: " + camera + ".")
    else:
        print("* Resolution: " + str(resolutionX) + " x " + str(resolutionY) + " pixels.")
        print("* Field of view: " + str(fovX) + " x " + str(fovY) + " rad.")

    print("")

if args.skipConfirmation or args.justPrint:
    confirmation = "y"
else:
    confirmation = input ("Do you want to proceed? (y/n): ")

#----------------------------------------------------------------------------------#

# Blender call

if confirmation.lower() == "y":
    # Runs blenderControl.py with the chosen parameters.
    # With this approach, everything has to be sent as strings, even vectors.
    # They are reinterpreted back in blenderControl.
    
    # Basic arguments that will always be passed on:
    mainArgs = [
        blenderPath,                        # Blender executable path.
        "--background",                     # Run Blender in the background.
        "asteroidImageGenerator.blend",     # Blender file to be run.
        "--python",                         # Execute a python script with the Blender file.
        "blenderControl.py",                # Python script file to be run.
    ]

    # Custom options:

    # This double dash marks the start of the custom options:
    options = ["--"]

    ## Common options

    if readFromFile:
        options += ["--fromFile"]
        options += ["--positionalData", positionalDataPath]

    else:
        options += ["--coordinates", str(sunCoordinates), str(cameraCoordinates)]
        options += ["--sunIrradiance", str(sunIrradiance)]

    if calibration:
        options += ["--calibration"]

    options += ["--function", str(function)]
    options += ["--functionName", functionList[function - 1]]

    options += ["--target", targetName]
    options += ["--proceduralDetail", str(proceduralDetail)]
    options += ["--systemRotation", str(systemRotation)]

    ## Camera specific options

    if camera == "ASPECT-VIS":

        options += [
                "--resolution", "1024", "1024",
                "--fov", "0.174533", "0.174533",    # 10 degrees in radians
                #"--multispectralWlRange", "[500, 900, 30]",
                ]

        if multispectral:

                options += ["--multispectralWlRange", "[500, 900, 30]"] # nm

        else:

                options += ["--wavelength", str(defaultWavelength)]

    elif camera == "ASPECT-NIR":

        options += [
                "--resolution", "640", "512",       # pixels
                "--fov", "0.116937", "0.0942478",   # 6.7 x 5.4 degrees in radians
                ]

        if multispectral:

                options += ["--multispectralWlRange", "[850, 1650, 30]"] # nm

        else:

                options += ["--wavelength", str(defaultWavelength)]


    elif camera == "ASPECT-VIS-Green":

        options += [
                "--resolution", "1024", "1024",     # pixels
                "--fov", "0.174533", "0.174533",    # 10 degrees in radians
                "--wavelength", "547",              # nm
                "--filter", "[0, 1, 0]",            # [R, G, B]
                ]

    else:
        """
        Default options for when no real camera is used.
        """

        # Render resolution
        options += ["--resolution", str(resolutionX), str(resolutionY)]

        # Field of view
        options += ["--fov", str(fovX), str(fovY)]

        # Multispectral
        if multispectral: 
            options += ["--multispectralWlRange", str(wavelengthRange)]

        else:
            options += ["--wavelength", str(defaultWavelength)]


    # Don't mind the next line, I'm using it to make the script fail and output the correct usage
    #options += ["--nothing"]

    if args.justPrint:
        print(' '.join(mainArgs + options))
    else:
        subprocess.run(mainArgs + options)
else:

    print("Closing")
