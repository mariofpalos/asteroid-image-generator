import bpy
import math
import mathutils
import os
import numpy as np

def sunSetupCartesian(sunObject, sun_x, sun_y, sun_z):
    """
    Updates the location and rotation of the sunlamp, with coordinates as input.
    
    Add power of the lamp, or distance and transform it into power
    """

    sunLocation = (sun_x, sun_y, sun_z)
    sunDirectionVector = mathutils.Vector(sunLocation)  # Assumes that the Sun points to (0,0,0)

    sunObject.location = sunLocation  # The location doesn't really matter with sunlamps
                                      # Distance should just modify the power of the lamp

    sunObject.rotation_mode = "XYZ"

    rot_quat = sunDirectionVector.to_track_quat("Z", "Y")
    sunObject.rotation_euler = rot_quat.to_euler()

def sunSetupSpherical():

    pass

def cameraSetupCartesian(cameraObject, camera_x, camera_y, camera_z):
    """
    Updates the location and rotation of the camera, with coordinates and
    a reference (is this the correct term in python?) to the camera as input.
    """

    cameraLocation = (camera_x, camera_y, camera_z)
    cameraDirectionVector = mathutils.Vector(cameraLocation)    # Assuming pointing to (0,0,0)

    cameraObject.location = cameraLocation

    cameraObject.rotation_mode = "XYZ"
    rot_quat = cameraDirectionVector.to_track_quat("Z", "Y")
    cameraObject.rotation_euler = rot_quat.to_euler()

def cameraSetupSpherical():

    pass

def targetSetup(targetName):
    """
    Loops around the "Targets" collection and hides every object except
    the chosen target, in both viewport and render views. 
    """

    targetsCollection = bpy.data.collections.get("Targets")

    parents = (x for x in targetsCollection.objects if not x.parent)

    for object in parents:

        if object.name == targetName:

            object.hide_viewport = False
            object.hide_render = False

            # And the same for its children, if it has any!
            if object.children:
                for child in object.children:
                    child.hide_viewport = False
                    child.hide_render = False

        else:
            # Children must me disabled when not in use   <-- lol

            object.hide_viewport = True
            object.hide_render = True
            
            if object.children:
                for child in object.children:
                    child.hide_viewport = True
                    child.hide_render = True

def proceduralDetail(targetName, state):
    """
    Activates/deactivates the procedural detail in the render.
    "state" should be a boolean.
    """

    target = bpy.data.objects[targetName]
    modifier = target.modifiers["GeometryNodes"]
    modifier.show_render = state

def systemRotation(targetName, angle):
    """
    Rotates the main body and its satellite. Angle in degrees, represents
    Didymoon's rotation. Didymain rotates accordingly.
    """

    targetObject = bpy.context.scene.objects[targetName]

    ratio = 5.27    # D2's orbital period / D1's rotation period.

    # First: rotate D1 by the angle times the ratio. 

    targetObject.rotation_euler[2] = math.radians(angle*ratio)

    # Second: rotate D2 by the desired angle.

    if targetObject.children:

        targetObject = targetObject.children[0]

    correction = angle - (angle*ratio)

    targetObject.rotation_euler[2] = math.radians(correction)

def sunIrradiance(sunObject, irradiance):
    """
    Sets the irradiance of the Sun lamp (in W/m^2) to the desired value.
    """

    sunObject.data.energy = irradiance

def sunDistanceToIrradiance(sunObject, distance):
    """
    Sets the power of the Sun lamp (in W/m^2) according to the distance to the Sun.
    Distance in meters?
    UNTESTED!
    """
    
    solarRadius = 6.957e8   # m
    solarSurfaceIrradiance = 64e6    # W/m²

    sunObject.data.energy = ( solarRadius ** 2 / distance ** 2 ) * solarSurfaceIrradiance

def sunSpectralIrradiance(wavelength):
    """
    Calculates the solar irradiance for the required wavelength.
    Data from 2000 ASTM Standard Extraterrestrial Spectrum Reference E-490-00.
    At 1 AU I think!
    """

    lambdas, spIrradiances = [], []
    filepath = os.path.join(os.path.dirname(bpy.data.filepath), 
            "data/spectralSolarIrradiance.txt")

    with open(filepath) as f:
        next(f)     # Skips the header
        next(f)     # Two lines in this case
        for line in f:
            items = [float(x) for x in line.split(',')]
            lambdas.append(items[0])
            spIrradiances.append(items[1])

    return np.interp(wavelength, lambdas, spIrradiances)

def wavelengthToAlbedo(wavelength):
    """
    Returns the albedo corresponding to a certain wavelength, based on data
    from S-type asteroids in the (450 - 2450) nm range
    """

    lambdas, albedos = [], []
    filepath = os.path.join(os.path.dirname(bpy.data.filepath), 
            "data/S-type-asteroid-Bus-DeMeo.txt")

    with open(filepath) as f:
        next(f)     # Skips the header
        for line in f:
            items = [float(x) for x in line.split('\t')]
            lambdas.append(items[0])
            albedos.append(items[1])

    return np.interp(wavelength, lambdas, albedos)
