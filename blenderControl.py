import bpy          
import mathutils 
import sys
import os
import argparse
import time

from pathlib import Path    # For generating folder, remove if not needed.

# Importing custom modules
# This first bit is necessary for importing custom python modules from inside Blender.
# It points Python to the location of the Blender file.
sys.path.append(os.path.dirname(bpy.data.filepath))
from modules import setup_functions, rendering_functions

#----------------------------------------------------------------------------------#

# ARGUMENT PARSING

argv = sys.argv

if "--" not in argv:
    argv = []  # as if no args are passed
else:
    # Get all args after "--" and saves them in a list
    argv = argv[argv.index("--") + 1:]

parser = argparse.ArgumentParser()

# Argument extraction.

parser.add_argument("--fromFile", dest = "fromFile", action = "store_true", required = False,
        help = "Reads positional data from external file")

parser.add_argument("--positionalData", dest = "positionalData", nargs = 1, required = False,
        help = "Location of the positional data for camera and Sun")

parser.add_argument("--spectralData", dest = "spectralData", nargs = 1, required = False,
        help = "Location of the spectral data file")

parser.add_argument("--coordinates", dest = "coordinates", nargs = 2, required = False,
        help = "Reads coordinates of sun and camera") 

parser.add_argument("--multispectralWlRange", dest = "multispectralWlRange", nargs = 1, 
        required = False,
        help = "Renders each frame of the sequence in a range of wavelengths.")

parser.add_argument("--wavelength", dest = "wavelength", nargs = 1, required = False,
        help = "Wavelength to be used if no multispectral render is requested")

parser.add_argument("--calibration", dest = "calibration", action = "store_true",
        help = "Asks for calibration images with a white Lambertian disk")

parser.add_argument("--function", dest = "function", nargs = 1, required = True,
        help = "Choice of photometric function")

parser.add_argument("--functionName", dest = "functionName", nargs = 1, required = True,
        help = "Name of the photometric function, used in the filename")

parser.add_argument("--target", dest = "target", nargs = 1, required = True,
        help = "Choice of target geometry")

parser.add_argument("--proceduralDetail", dest = "proceduralDetail", nargs = 1, required = True,
        help = "State of the procedural detail modifier")

parser.add_argument("--systemRotation", dest = "systemRotation", nargs = 1, required = True,
        help = "Rotation of the system, in degrees")

parser.add_argument("--resolution", dest = "resolution", nargs = 2, required = True,
        help = "Resolution of the final renders")

parser.add_argument("--fov", dest = "fov", nargs = 2, required = True,
        help = "Field of view size in degrees")

parser.add_argument("--filter", dest = "filter", nargs = 1, required = False,
        help = "Multipliers for the RGB channels, between 0 and 1.")

parser.add_argument("--sunIrradiance", dest = "sunIrradiance", nargs = 1, required = False,
        help = "Sun irradiance.")

parser.add_argument("--outDir", dest = "outDir", nargs = 1, required = False,
        help = "Output directory for renders, ovverwite default scheme with OBJ-FUNC-DATE-TIME.")

args = parser.parse_args(argv)
print(args)	

#----------------------------------------------------------------------------------#

# Scene setup

## Move to frame 1, just in case.
scene = bpy.context.scene
bpy.context.scene.frame_set(1)

## Target setup
setup_functions.targetSetup(args.target[0])

## Procedural detail

# Translate the string to booleans
if args.proceduralDetail[0] == "True":
    state = True
else:
    state = False

# Try/Except, in case the object doesn't have a geometry nodes modifier
try:
    setup_functions.proceduralDetail(args.target[0], state)
except:
    pass

"""
# Moving this down to the file/not file section
## Target rotation
setup_functions.systemRotation(args.target[0], int(args.systemRotation[0]))
"""

## Material setup
mat = bpy.data.materials["Surface Material"]
nodes = mat.node_tree.nodes

# Pebble Material is essentially the same, but with random albedo per object.
matPebbles = bpy.data.materials["Pebble Material"]
nodesPebbles = matPebbles.node_tree.nodes

# Finds the node called "Function" and sets its "Value" to that of the chosen function.
functionNode = nodes.get("Function")
functionNode.outputs["Value"].default_value = int(args.function[0])

# And again for the pebbles, I guess?
functionNodePebbles = nodes.get("Function")
functionNodePebbles.outputs["Value"].default_value = int(args.function[0])

## Albedo
albedoNode = nodes.get("Albedo")
albedoNodePebbles = nodesPebbles.get("Albedo")

## Sun and camera

camera = bpy.data.objects["Main Camera"]
sun = bpy.data.objects["Sun"]

## Camera angle
# This works, but I'd prefer to find the camera by name

bpy.data.cameras[0].angle_x = float(args.fov[0])
bpy.data.cameras[0].angle_y = float(args.fov[1])

## Filter

if args.filter:
    # Extract individual values from the string.

    RGB = [float(i.strip()) for i in args.filter[0][1:-1].split(",")]
    (RValue, GValue, BValue) = RGB

    # Set the values of the RGB nodes.

    nodes.get("R").outputs["Value"].default_value = RValue
    nodes.get("G").outputs["Value"].default_value = GValue
    nodes.get("B").outputs["Value"].default_value = BValue

else:
    # Needed?
    nodes.get("R").outputs["Value"].default_value = 1
    nodes.get("G").outputs["Value"].default_value = 1
    nodes.get("B").outputs["Value"].default_value = 1

## Coordinates and illumination. And rotation.

if not args.fromFile:
    # Set the coordinates from the user input, the illumination (?),
    # and set the "animation" to a length of one.

    # Sets the length of the animation to one frame.
    # This is simpler than writing specific code for rendering one image.
    scene.frame_end = 1

    # Reading sun coordinates from the command line arguments and setting the sun up.
    sunCoordinates = [float(i.strip()) for i in args.coordinates[0][1:-1].split(",")]
    (sun_x, sun_y, sun_z) = sunCoordinates
    setup_functions.sunSetupCartesian(sun, sun_x, sun_y, sun_z)

    if not args.sunIrradiance:
        args.sunIrradiance[0]=180;

    setup_functions.sunIrradiance(sun, float(args.sunIrradiance[0]))

    # Reading camera coordinates from the command line arguments and setting the camera up.
    cameraCoordinates = [float(i.strip()) for i in args.coordinates[1][1:-1].split(",")]
    (camera_x, camera_y, camera_z) = cameraCoordinates
    setup_functions.cameraSetupCartesian(camera, camera_x, camera_y, camera_z)

    ## Target rotation
    setup_functions.systemRotation(args.target[0], int(args.systemRotation[0]))

else:
    # Loops through the file and adds new animation keyframes
        
    # Assuming the "data" folder is in the same directory as the blender file:
    filepath = os.path.join(os.path.dirname(bpy.data.filepath), args.positionalData[0])

    with open(filepath) as f:
        lines = f.readlines() # Stores the file as a list of strings

    startFrame = 1
    endFrame = len(lines[1:])
    scene.frame_end = endFrame # Sets the end frame to the size of the dataset

    frame = 1

    for line in lines[1:]:      # [1:] skips the first line (that contains column names)

        # Move the scene to the appropriate frame
        scene.frame_set(frame)

        # Split the data string on its whitespaces
        line = line.split()

        # New: set the rotation of the system
        setup_functions.systemRotation(args.target[0], float(line[7]))

        # AH!!! AND KEYFRAME IT!!
        targetObject = bpy.context.scene.objects[args.target[0]]
        targetObject.keyframe_insert(data_path = "rotation_euler", frame = frame)

        if targetObject.children:

            childObject = targetObject.children[0]
            childObject.keyframe_insert(data_path = "rotation_euler", frame = frame)

        # Assign values to variables
        camera_x = float(line[0])
        camera_y = float(line[1])
        camera_z = float(line[2])

        # Setup the camera position and point it to the target
        setup_functions.cameraSetupCartesian(camera, camera_x, camera_y, camera_z)

        # And keyframe both the position and the rotation 
        camera.keyframe_insert(data_path = "location", frame = frame)
        camera.keyframe_insert(data_path = "rotation_euler", frame = frame)

        # Same procedure for the Sun, plus a keyframe for the irradiance.
        sun_x = float(line[3])
        sun_y = float(line[4])
        sun_z = float(line[5])

        setup_functions.sunSetupCartesian(sun, sun_x, sun_y, sun_z)
        setup_functions.sunIrradiance(sun, float(line[6]))

        sun.keyframe_insert(data_path = "location", frame = frame)
        sun.keyframe_insert(data_path = "rotation_euler", frame = frame)
        sun.data.keyframe_insert(data_path = "energy", frame = frame)


        # And advance one frame
        frame += 1

# Sets the camera as the render camera, because sometimes the
# targets are set as cameras after you add them for some reason.
scene.camera = camera

# Move to frame 1 again.
bpy.context.scene.frame_set(1)

#----------------------------------------------------------------------------------#

#RENDERING

resolutionX = int(args.resolution[0])
resolutionY = int(args.resolution[1])

if args.outDir:
  foldername = args.outDir[0] 
else:
    # Renders all the (animation) frames in a new folder inside /Renders.
    foldername = ("//Renders/" + args.target[0] + "-" + args.functionName[0] + "-" +
            time.strftime("%d%m%Y-%H%M%S"))

    """
    # Doesn't work if the folder doesn't exist yet.
    with open(foldername[2:] + "/parameters.txt", "w") as file:
        file.write(str(args))
    """

if args.multispectralWlRange:
    # Renders one image per frame and wavelength, in a range selected by the user
    
    # Read args.multispectralWlRange and extract the values into a vector.
    wlRange = [float(i.strip()) for i in args.multispectralWlRange[0][1:-1].split(",")]
    (start, stop, step) = wlRange
    
    wavelength = start

    while wavelength <= stop:
        # Loops over the list and renders one animation per wavelength value.

        filename = str(wavelength) + "nm"

        # Modify the albedo according to the wavelength
        albedoNode.outputs["Value"].default_value = \
                setup_functions.wavelengthToAlbedo(wavelength)

        albedoNodePebbles.outputs["Value"].default_value = \
                setup_functions.wavelengthToAlbedo(wavelength)


        """
        # Modify the solar irradiance according to the wavelength.
        # Not ready, need to scale the data or something, everything's too dark
        # (But it works)

        irradiance = setup_functions.sunSpectralIrradiance(wavelength)      
        setup_functions.sunIrradiance(sun, irradiance)
        """

        # render the animation,
        rendering_functions.renderAnimation(scene, resolutionX, resolutionY,
            foldername, filename)

        # Calibration
        if args.calibration:
            # Changes the visible object to the calibration disk
            setup_functions.targetSetup("CalibrationDisk")
            # Renders the complete scene again
            rendering_functions.renderAnimation(scene, resolutionX, resolutionY,
                foldername, filename + "-calibration")
            # And shows the original body back
            setup_functions.targetSetup(args.target[0])

        # and increase the counter
        wavelength += step

else:

    # Renders each frame once with the albedo corresponding to the default WAVELENGTH.
    albedoNode.outputs["Value"].default_value = \
            setup_functions.wavelengthToAlbedo(args.wavelength[0])

    albedoNodePebbles.outputs["Value"].default_value = \
            setup_functions.wavelengthToAlbedo(args.wavelength[0])

    #setup_functions.sunIrradiance(sun, 250) # For Antti's stuff!
    #setup_functions.sunIrradiance(sun, 150)
    #setup_functions.sunIrradiance(sun, float(args.sunIrradiance[0])) 

    #filename = "frame"      # find a better name!

    #more files in the same directory - different name by parameter
    if args.outDir:
        filename = str(args.wavelength[0]) + "nm" + "-si" + str(sunIntensity) + "-sun" + str(sun_x) + "_" + str(sun_y) + "_" + str(sun_z) + "-camera" + str(camera_x) + "_" + str(camera_y) + "_" + str(camera_z)

    else:
        filename = "frame"      # find a better name!


    rendering_functions.renderAnimation(scene, resolutionX, resolutionY,
            foldername, filename)

    if args.calibration:
        # Changes the visible object to the calibration disk
        setup_functions.targetSetup("CalibrationDisk")
        # Renders the complete scene again
        rendering_functions.renderAnimation(scene, resolutionX, resolutionY,
            foldername, filename + "-calibration")
        # And shows the original body back
        setup_functions.targetSetup(args.target[0])

if not args.outDir:
    # Here I save the args used to a txt file for future reference.
    """
    with open(foldername[2:] + "/parameters.txt", "w") as file:
        file.write(str(args))
    """
print("Script finished.")
